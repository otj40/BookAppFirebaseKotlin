package kar.ru.otj40.bookappkotlin

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kar.ru.otj40.bookappkotlin.databinding.ActivityRegisterBinding

class RegisterActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegisterBinding

    private lateinit var firebaseAuth: FirebaseAuth

    private lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        firebaseAuth = FirebaseAuth.getInstance()

        progressDialog = ProgressDialog(this)
        progressDialog.setTitle("Please wait...")
        progressDialog.setCanceledOnTouchOutside(false)

        binding.backArrowBtn.setOnClickListener {
            onBackPressed()
        }

        binding.registerBtn.setOnClickListener {
            /*Steps
            (1)Input Data
            (2)Validate Data
            (3)Create Account - FB Auth
            (4)Save User Data - FB Realtime Database
             */
            validateData()
        }
    }

    private var name = ""
    private var email = ""
    private var password = ""
    private fun validateData() {
        // (1)Input Data
        name = binding.nameEtRegister.text.toString().trim()
        email = binding.emailEtRegister.text.toString().trim()
        password = binding.passwordEtRegister.text.toString().trim()
        val confPassword = binding.confirmPasswordEtRegister.text.toString().trim()

        // (2)Validate Data
        if (name.isEmpty()) toastString("Enter Your Name...")
        else if (!Patterns.EMAIL_ADDRESS.matcher(email)
                .matches()
        ) toastString("Invalid Email pattern...")
        else if (password.isEmpty()) toastString("Enter Password...")
        else if (confPassword.isEmpty()) toastString("Confirm Password...")
        else if (password != confPassword) toastString("Passwords don`t match")
        else createAccount()
    }

    private fun createAccount() {
        // (3)Create Account - FB Auth
        progressDialog.setMessage("Creating Account...")
        progressDialog.show()

        firebaseAuth.createUserWithEmailAndPassword(email, password)
            .addOnSuccessListener {
                updateUserInfo()
            }
            .addOnFailureListener {
                progressDialog.dismiss()
                toastString("failed due to ${it.message}")
            }
    }

    private fun updateUserInfo() {
        // (4)Save User Data - FB Realtime Database
        progressDialog.setMessage("Saving User Info...")

        val timestamp = System.currentTimeMillis()

        val uid = firebaseAuth.uid

        //setup data to add in DB
        val hashMap: HashMap<String, Any?> = HashMap()
        hashMap["uid"] = uid
        hashMap["email"] = email
        hashMap["name"] = name
        hashMap["profileImage"] = "" //add empty, will do in profile edit
        hashMap["userType"] = "user" // possible values user/admin, will set manually
        hashMap["timestamp"] = timestamp

        val ref = FirebaseDatabase.getInstance().getReference("Users")
        ref.child(uid!!).setValue(hashMap)
            .addOnSuccessListener {
                progressDialog.dismiss()
                toastString("Account Created...")
                startActivity(Intent(this@RegisterActivity, DashboardUserActivity::class.java))
                finish()
            }
            .addOnFailureListener {
                toastString("failed due to ${it.message}")
            }
    }

    private fun toastString(string: String) {
        Toast.makeText(this, string, Toast.LENGTH_SHORT).show()
    }
}